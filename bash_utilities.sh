### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

# --------------------------------------------------------------------
# description
# --------------------------------------------------------------------

### Source me for bash convenience functions: notably, ensuring system/environment dependencies are available.
### Note this assumes linux platform: dependencies include * `hostname`

# --------------------------------------------------------------------
# code
# --------------------------------------------------------------------

# --------------------------------------------------------------------
# constants
# --------------------------------------------------------------------

### TODO: take switches for help, debugging, no/eval, target drive

# Following does not work with `source`
# THIS="$0"
# THIS_FN="$(basename ${THIS})"
# THIS_DIR="$(dirname ${THIS})"

### constants by model/run
# TODO: read IOAPI info from CCTM Makefile
IOAPI_VERSION="3.1" # desired regardless of host

### constants by host/cluster

## tlrPanP5
# HDF5 version per http://www.hdfgroup.org/hdf5-quest.html#vers1 :
# `find /usr/lib/ | fgrep -ie 'libhdf5.a' | xargs strings | fgrep -ne 'HDF5 library version:'`
TLRP_HDF5_VERSION='1.8.8'
TLRP_NCL_VERSION="ncl_ncarg-6.1.1.Linux_Debian6.0_x86_64_nodap_gcc445"
TLRP_NETCDF_VERSION='4.1.3' # AND I NOW HAVE R PACKAGE=ncdf4 !!!
TLRP_R_VERSION='2.15.1'

# NCARG_ROOT required by NCL
TLRP_NCARG_ROOT="${HOME}/bin/${TLRP_NCL_VERSION}"
TLRP_NCL_PATH="${TLRP_NCARG_ROOT}/bin"
TLRP_R_PATH='/usr/bin'

## HPCC
# a much less regular environment than EMVL :-(
HPCC_NCO_VERSION='4.4.0' # on infinity, anyway
AMAD1_NCL_VERSION='ncl_ncarg-6.1.2.Linux_RHEL5.6_x86_64_nodap_gcc412'
INFINITY_NCL_VERSION='ncl_ncarg-6.2.0.Linux_RHEL5.10_x86_64_nodap_gcc412'

#HPCC_IOAPI_LIB_PATH="/project/air5/roche/CMAQ-5-eval/lib/ioapi_${IOAPI_VERSION}"
HPCC_IOAPI_LIB_PATH="/home/wdx/lib/x86_64i/intel/ioapi_${IOAPI_VERSION}/Linux2_x86_64ifort"
HPCC_IOAPI_BIN_PATH="${HPCC_IOAPI_LIB_PATH}"

# `ncdump` now on hpcc in /usr/bin
#HPCC_NCDUMP_PATH='/share/linux86_64/grads/supplibs-2.2.0/x86_64-unknown-linux-gnu/bin'
HPCC_NCO_PATH="/share/linux86_64/nco/nco-${HPCC_NCO_VERSION}/bin"
HPCC_R_PATH='/share/linux86_64/bin'

AMAD1_NCARG_ROOT="${HOME}/bin/${AMAD1_NCL_VERSION}"
AMAD1_NCL_PATH="${AMAD1_NCARG_ROOT}/bin"

INFINITY_NCARG_ROOT="${HOME}/bin/${INFINITY_NCL_VERSION}"
INFINITY_NCL_PATH="${INFINITY_NCARG_ROOT}/bin"

## terrae/EMVL
## Note terrae hosts use Environment Modules http://modules.sourceforge.net/
## To see which modules you have loaded: `module list`
## To see which modules are available: `module avail`

# terrae/EMVL versions (as of Dec 2013)

# TERRAE_GCC_VERSION='4.1.2'
TERRAE_GCC_VERSION='4.6'

# needed after updates to R packages on terrae 25 Apr 2013
TERRAE_GDAL_VERSION='1.9.2/intel-13.0'

TERRAE_INTEL_VERSION='13.1'

TERRAE_IOAPI_VERSION="ioapi-${IOAPI_VERSION}/intel-${TERRAE_INTEL_VERSION}"

#TERRAE_HDF5_VERSION='1.8.7'
#TERRAE_HDF5_VERSION="1.8.10/intel-13.0'
TERRAE_HDF5_VERSION="1.8.11/intel-${TERRAE_INTEL_VERSION}"

#TERRAE_NCO_VERSION='4.0.5'
#TERRAE_NCO_VERSION='4.2.3/${TERRAE_GCC_MODULE}'
#TERRAE_NCO_VERSION='4.2.3'
TERRAE_NCO_VERSION='4.2.3/intel-13.0'

#TERRAE_NETCDF_VERSION='4.1.2'
#TERRAE_NETCDF_VERSION='4.1.2/intel-13.0'
TERRAE_NETCDF_VERSION="4.3.0/intel-${TERRAE_INTEL_VERSION}"

#TERRAE_NCL_VERSION='ncl_ncarg-6.1.2.Linux_RHEL5.6_x86_64_gcc412'
TERRAE_NCL_VERSION='6.1.2'

# needed after updates to R packages on terrae 25 Apr 2013
TERRAE_PROJ4_VERSION='4.8.0/intel-13.0'

#TERRAE_R_VERSION='2.15.2'
TERRAE_R_VERSION='3.0.0'

# terrae/EMVL modules

# needed after updates to R packages on terrae 25 Apr 2013
TERRAE_GDAL_MODULE="gdal-${TERRAE_GDAL_VERSION}"

TERRAE_GCC_MODULE="gcc-${TERRAE_GCC_VERSION}"

TERRAE_HDF5_MODULE="hdf5-${TERRAE_HDF5_VERSION}"

TERRAE_INTEL_MODULE="intel-${TERRAE_INTEL_VERSION}"

#TERRAE_IOAPI_MODULE="ioapi-${IOAPI_VERSION}/${TERRAE_GCC_MODULE}"
#TERRAE_IOAPI_MODULE="ioapi-${IOAPI_VERSION}"
TERRAE_IOAPI_MODULE="ioapi-${TERRAE_IOAPI_VERSION}"

#TERRAE_NCL_VERSION='6.0.0' # outdated
# NCARG_ROOT required by NCL
# use Herwehe's while EMVL upgrades RHEL
#TERRAE_NCARG_ROOT="/home/hhg/ncl_ncarg/${TERRAE_NCL_VERSION}"
TERRAE_NCL_MODULE="ncl_ncarg-${TERRAE_NCL_VERSION}"

TERRAE_NCO_MODULE="nco-${TERRAE_NCO_VERSION}"

TERRAE_NETCDF_MODULE="netcdf-${TERRAE_NETCDF_VERSION}"

# needed after updates to R packages on terrae 25 Apr 2013
TERRAE_PROJ4_MODULE="proj-${TERRAE_PROJ4_VERSION}"

#TERRAE_R_MODULE='R'
# note different separator
TERRAE_R_MODULE="R/${TERRAE_R_VERSION}"

# ----------------------------------------------------------------------
# functions
# ----------------------------------------------------------------------

# Ensure various needed artifacts are on various *xy paths,
# but assumes `hostname` is already available.
# TODO: ensure your hostname matches here!
# TODO: setup packages={ncdf4} on infinity, not just amad
function setup_paths {
  H="$(hostname)"
  case "${H}" in
    amad*)
      echo -e "${FUNCNAME[0]}: ${H} is on HPCC"
# as of 22 May 12, on the HPCC R servers NCO is installed normally, in /usr/bin
#      addPath "${HPCC_NCO_PATH}"
      addPath "${HPCC_IOAPI_BIN_PATH}"
      export NCARG_ROOT="${AMAD1_NCARG_ROOT}"
      addPath "${AMAD1_NCL_PATH}"
      addPath "${HPCC_R_PATH}"
      addLdLibraryPath "${HPCC_IOAPI_LIB_PATH}"
      ;;
    global*)
      echo -e "${FUNCNAME[0]}: ${H} is on HPCC"
#      addPath "${HPCC_NCO_PATH}"
      addPath "${HPCC_IOAPI_BIN_PATH}"
      addPath "${HPCC_R_PATH}"
      addLdLibraryPath "${HPCC_IOAPI_LIB_PATH}"
      ;;
    imaster*) # == infinity
      echo -e "${FUNCNAME[0]}: ${H} is on HPCC"
      echo -e "For R packages such as ncdf4, must run on amad"
      addPath "${HPCC_NCO_PATH}"
      export NCARG_ROOT="${INFINITY_NCARG_ROOT}"
      addPath "${INFINITY_NCL_PATH}"
      addPath "${HPCC_IOAPI_BIN_PATH}"
      addPath "${HPCC_R_PATH}"
      addLdLibraryPath "${HPCC_IOAPI_LIB_PATH}"
      ;;
    inode*) # == node39
      echo -e "${FUNCNAME[0]}: ${H} is on HPCC"
      echo -e "For R packages such as ncdf4, must run on amad"
      addPath "${HPCC_NCO_PATH}"
      addPath "${HPCC_IOAPI_BIN_PATH}"
      addPath "${HPCC_R_PATH}"
      addLdLibraryPath "${HPCC_IOAPI_LIB_PATH}"
      ;;
    terra*)
      echo -e "${FUNCNAME[0]}: ${H} is on terrae"
      removeModule 'intel' # required on terrae/EMVL as of 23 Apr 2013
      addModule "${TERRAE_GCC_MODULE}"
      addModule "${TERRAE_INTEL_MODULE}"
      addModule "${TERRAE_GDAL_MODULE}"
      addModule "${TERRAE_PROJ4_MODULE}"
      addModule "${TERRAE_HDF5_MODULE}"
      addModule "${TERRAE_NETCDF_MODULE}"
      addModule "${TERRAE_NCL_MODULE}"
      addModule "${TERRAE_NCO_MODULE}"
      addModule "${TERRAE_R_MODULE}"
      echo -e "${FUNCNAME[0]}: 'module list -t'=="
      modulecmd bash list -t > ${TEMPFILE}
      source ${TEMPFILE}
#      # until NCL is module'd on terrae
#      export NCARG_ROOT="${TERRAE_NCARG_ROOT}"
#      addPath "${TERRAE_NCARG_ROOT}/bin"
      ;;
    tlr*)
      echo -e "${FUNCNAME[0]}: ${H} is a Tom box"
      # NCL
      export NCARG_ROOT="${TLRP_NCARG_ROOT}"
      addPath "${TLRP_NCL_PATH}"
      addPath "${TLRP_R_PATH}"
      ;;
    *)
      echo -e "${FUNCNAME[0]}: unknown ${H}"
#      exit 1
      ;;
  esac
} # end function setup_paths

# Choose which app to use based on host. Assumes
# * paths (et al) are properly configured by setup_paths (above)
# * `hostname` is available
# TODO: ensure your hostname matches here!
function setup_apps {
  H="$(hostname)"
  case "${H}" in
    amad*)
      echo -e "${FUNCNAME[0]}: ${H} is on HPCC"
      # note evince is *installed* on amad1, but will fail to run due to X problems
      export PDF_VIEWER='evince'
      ;;
    global*)
      echo -e "${FUNCNAME[0]}: ${H} is on HPCC"
      export PDF_VIEWER='xpdf'
      ;;
    imaster*) # == infinity
      echo -e "${FUNCNAME[0]}: ${H} is on HPCC/infinity"
      export PDF_VIEWER='evince'
      ;;
    inode*) # == node39
      echo -e "${FUNCNAME[0]}: ${H} is on HPCC"
      export PDF_VIEWER='xpdf'
      ;;
    sol*)
      echo -e "${FUNCNAME[0]}: ${H} is on EMVL/sol"
      export PDF_VIEWER='xpdf'
      ;;
    terra*)
      echo -e "${FUNCNAME[0]}: ${H} is on EMVL/terrae"
      export PDF_VIEWER='xpdf'
      ;;
    tlr*)
      echo -e "${FUNCNAME[0]}: ${H} is a Tom box"
      export PDF_VIEWER='evince'
      ;;
    *)
      echo -e "${FUNCNAME[0]}: unknown ${H}"
#      exit 1
      ;;
  esac
} # end function setup_apps

# add $1 to PATH if not already there
function addPath {
    DIR="$1"
    if [[ -n "${DIR}" ]] ; then
      if [[ -d "${DIR}" ]] ; then
        if [[ ":${PATH}:" != *":${DIR}:"* ]] ; then
          PATH="${DIR}:${PATH}"
        else
          echo -e "${FUNCNAME[0]}: PATH contains '${DIR}'"
        fi
      else
        echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: '${DIR}' is not a directory" 1>&2
      fi
    else
      echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: DIR not defined" 1>&2
    fi
}

# add $1 to LD_LIBRARY_PATH if not already there
function addLdLibraryPath {
    DIR="$1"
    if [[ -n "${DIR}" ]] ; then
      if [[ -d "${DIR}" ]] ; then
        if [[ ":${LD_LIBRARY_PATH}:" != *":${DIR}:"* ]] ; then
          LD_LIBRARY_PATH="${DIR}:${LD_LIBRARY_PATH}"
        else
          echo -e "LD_LIBRARY_PATH contains '${DIR}'"
        fi
      else
        echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: '${DIR}' is not a directory" 1>&2
      fi
    else
      echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: DIR not defined" 1>&2
    fi
}

# add module=$1 to your  Environment Modules (
# http://modules.sourceforge.net/
# TODO: check for existence of module
function addModule {
  MODULE="$1"
  TEMPFILE="$(mktemp)"
  modulecmd bash add ${MODULE} > ${TEMPFILE}
  source ${TEMPFILE}
}

# remove module=$1 from your  Environment Modules (
# http://modules.sourceforge.net/
# TODO: check for existence of module
function removeModule {
  MODULE="$1"
  TEMPFILE="$(mktemp)"
  modulecmd bash rm ${MODULE} > ${TEMPFILE}
  source ${TEMPFILE}
}

# # If your computing platform uses Environment Modules (
# # http://modules.sourceforge.net/
# # ), load modules for current NCO and IOAPI, noting
# # how this syntax differs from the commandline.
# # (Thanks, Barron Henderson for noting this.)
# # TODO: test for non/existence of paths above!
# function setupModules {
#   # for CMD in \
#   #   "modulecmd bash add ${TERRAE_NCO_MODULE} ${TERRAE_IOAPI_MODULE}" \
#   # ; do
#   #   echo -e "$ ${CMD}"
#   #   eval "${CMD}"
#   # done
#   TEMPFILE="$(mktemp)"
#   modulecmd bash add ${TERRAE_NCO_MODULE} ${TERRAE_IOAPI_MODULE} > ${TEMPFILE}
#   source ${TEMPFILE}
# }

# "Comments" lines from running iff _DEBUG='on' (which can be export'ed by caller),
# and runs with `set xtrace`
# For `echo`, use DEBUG()
function DEBUGx {
  if [[ "${_DEBUG}" == 'on' ]] ; then
    set -x
    "$@" 1>&2
    set +x
  fi
} # end function DEBUG

# "Comments" lines from running iff _DEBUG='on'
# (which can be export'ed by caller)
function DEBUG {
  if [[ "${_DEBUG}" == 'on' ]] ; then
    "$@" 1>&2
  fi
} # end function DEBUG
