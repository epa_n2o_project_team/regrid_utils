### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

### miscellaneous R codes used in regridding projects
### TODO: package my code! see howto, e.g., Wickham project=devtools

# workaround apparent bug in ncdf4
get.ncdf4.prec <- function(
  ncvar4 # object of type=ncvar4
) {
  # TODO: test argument
  ncdf4.prec <- ncvar4$prec
  if (ncdf4.prec == "int") {
    return("integer")
  } else {
    return(ncdf4.prec)
  }
}

### Test if argument is zero within tolerance
is.zero.tol <- function (val, tol = getOption("tolerance")) {
  if (is.null(tol)) {
    tol <- 1e-11
  }
  abs(val) <= tol
}

### Test if argument is NA or zero within tolerance
is.arr.na.or.zero <- function(arr) {
# # start debugging-----------------------------------------------------
#  base::cat(this_fn, '::is.arr.na.or.zero: arr==\n')
#  print(arr)
# #   end debugging-----------------------------------------------------
  # bitwise 'or' !
  is.na(c(arr)) | is.zero.tol(c(arr))
} # end is.arr.na.or.zero

### FOR USE ONLY IN `if` CONDITIONS:
### Return TRUE if value is NA or 0, else FALSE.
### ASSERT: input is numeric (we won't check!)
is.val.na.or.zero <- function(val) {

# # start debugging-----------------------------------------------------
#  base::cat(this_fn, '::is.val.na.or.zero: val==\n')
#  print(val)
# #   end debugging-----------------------------------------------------

  if (is.na(val)) {
    TRUE
  } else {
    # TODO: add optional tolerance
    if (val == 0) {
      TRUE
    } else {
      FALSE
    }
  }
} # end is.val.na.or.zero

### FOR USE ONLY IN `if` CONDITIONS, when dealing with data like
### >  [1] NA NA NA NA  0 NA NA NA NA NA NA NA NA NA NA NA NA NA NA NA NA NA NA NA NA
### > [26] NA NA NA NA NA NA NA NA NA NA NA  0  0 NA NA NA NA
### Return TRUE if all elements of a vector are either NA or 0.
### ASSERT: input is numeric (we won't check!)
is.vec.na.or.zero <- function(vec) {

# # start debugging-----------------------------------------------------
#  base::cat(this_fn, '::is.vec.na.or.zero: vec==\n')
#  print(vec)
# #   end debugging-----------------------------------------------------

  if        (!sum(!is.na(vec))) {
    TRUE
  } else if (sum(subset(vec, !is.na(vec))) == 0) {
    TRUE
  } else {
    FALSE
  }
} # end is.vec.na.or.zero
