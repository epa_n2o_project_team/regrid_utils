#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

### Just a utility to scan status of regridding repos: NOT part of the build/run.
### Looks for files in peer repos with same names as in regrid_utils, and (slightly) compares each to regrid_utils counterpart.
### Note similarity to repo*.sh in repos={this, CMAQ-build}. TODO: refactor.

THIS="$0"
THIS_DIR="$(readlink -f $(dirname ${THIS}))"
THIS_FN="$(basename ${THIS})"
MESSAGE_PREFIX="${THIS_FN}:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

REPO_ROOT="${HOME}/code/regridding" # on terrae
RU_DIR="${REPO_ROOT}/regrid_utils"  # ... path to repo=regrid_utils

for REPO_DIR in $(find ${REPO_ROOT}/ -maxdepth 1 -type d | grep -ve '/$' | grep -ve 'BLD_\|RUN_' | sort) ; do
  pushd ${REPO_DIR} > /dev/null
  REPO_NAME="$(basename ${REPO_DIR})"
  echo -e "${MESSAGE_PREFIX} in ${REPO_NAME}:"

  # Note difference between following command and REPO_CMD in other files
  # Parse uncommitted in porcelain format="?? <file path/>" where '?' are literal.
  for CANDIDATE_FN in $(git status -u --porcelain | grep -ve '~$\|\.gz$\|\.log$\|\.nc$\|\.ncf$\|\.pdf$\|\.txt$\|\.zip$') ; do
    # remember, results are space-delimited! so will be split ...
    if [[ "${CANDIDATE_FN}" != '??' ]] ; then
      RU_FP="${RU_DIR}/${CANDIDATE_FN}"
      if [[ -e "${RU_FP}" ]] ; then
        CANDIDATE_FP="${REPO_DIR}/${CANDIDATE_FN}"
        CANDIDATE_SUBPATH="${REPO_NAME}/${CANDIDATE_FN}"
        DIFF_CMD="diff -uwB ${RU_FP} ${CANDIDATE_FP}"
        N_DIFF_LINES="$(${DIFF_CMD} | wc -l)"
        if (( ${N_DIFF_LINES} > 0 )) ; then
          if [[ "${CANDIDATE_FP}" -nt "${RU_FP}" ]] ; then
            OUT_PREFIX='\tnewer\t'
          else
            OUT_PREFIX='\t'
          fi
#          echo -e "${OUT_PREFIX}${CANDIDATE_SUBPATH}"
          # more useful output (cut'n'paste)
          echo -e "${OUT_PREFIX}${DIFF_CMD}"
        fi # if (( ${N_DIFF_LINES} > 0 ))
      else
        echo -e "${MESSAGE_PREFIX} ${CANDIDATE_FN} not found in ${RU_DIR}"
      fi # if [[ -e "${RU_FP}" ]]
    fi # if [[ "${CANDIDATE_FN}" != '??' ]]
  done # for CANDIDATE_FN

  popd > /dev/null
  echo # newline
done # for REPO_DIR
