### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

### R code for depicting Raster*, whether single- or multi-layer.
### REQUIRES netCDF.stats.to.stdout.r, e.g.,
### source(stat_funcs_fp)
### TODO: package my code! see howto, e.g., Wickham project=devtools

# database: Geographical database to use.  Choices include "state"
#           (default), "world", "worldHires", "canusamex", etc.  Use
#           "canusamex" to get the national boundaries of the Canada, the
#           USA, and Mexico, along with the boundaries of the states.
#           The other choices ("state", "world", etc.) are the names of
#           databases included with the ‘maps’ and ‘mapdata’ packages.

project.M3.boundaries.for.CMAQ <- function(
  database='state', # see `?M3::get.map.lines.M3.proj`
  units='m',        # or 'km': see `?M3::get.map.lines.M3.proj`
  extents.fp,       # path to extents file
  # TODO: calculate extents from extents.fp if extents==null
  extents,          # raster::extent object
  # TODO: calculate LCC.parallels from if LCC.parallels==null
  LCC.parallels=c(33,45), # LCC standard parallels: see https://github.com/TomRoche/cornbeltN2O/wiki/AQMEII-North-American-domain#wiki-EPA
  CRS               # see `sp::CRS`
) {

  library(M3)
  ## Will replace raw LCC map's coordinates with:
  metadata.coords.IOAPI.list <- M3::get.grid.info.M3(extents.fp)
  metadata.coords.IOAPI.x.orig <- metadata.coords.IOAPI.list$x.orig
  metadata.coords.IOAPI.y.orig <- metadata.coords.IOAPI.list$y.orig
  metadata.coords.IOAPI.x.cell.width <- metadata.coords.IOAPI.list$x.cell.width
  metadata.coords.IOAPI.y.cell.width <- metadata.coords.IOAPI.list$y.cell.width

  library(maps)
  map.lines <- M3::get.map.lines.M3.proj(
    file=extents.fp, database=database, units="m")
  # dimensions are in meters, not cells. TODO: take argument
  map.lines.coords.IOAPI.x <-
    (map.lines$coords[,1] - metadata.coords.IOAPI.x.orig)
  map.lines.coords.IOAPI.y <-
    (map.lines$coords[,2] - metadata.coords.IOAPI.y.orig)
  map.lines.coords.IOAPI <- 
    cbind(map.lines.coords.IOAPI.x, map.lines.coords.IOAPI.y)

  # # start debugging
  # class(map.lines.coords.IOAPI)
  # # [1] "matrix"
  # summary(map.lines.coords.IOAPI)
  # #  map.lines.coords.IOAPI.x map.lines.coords.IOAPI.y
  # #  Min.   : 283762                Min.   : 160844               
  # #  1st Qu.:2650244                1st Qu.:1054047               
  # #  Median :3469204                Median :1701052               
  # #  Mean   :3245997                Mean   :1643356               
  # #  3rd Qu.:4300969                3rd Qu.:2252531               
  # #  Max.   :4878260                Max.   :2993778               
  # #  NA's   :168                    NA's   :168             
  # #   end debugging

  # Note above is not zero-centered, like our extents:
  # extent : -2556000, 2952000, -1728000, 1860000  (xmin, xmax, ymin, ymax)
  # So gotta add (xmin, ymin) below.

  ## Get LCC state map
  # see http://stackoverflow.com/questions/14865507/how-to-display-a-projected-map-on-an-rlatticelayerplot
  map.IOAPI <- maps::map(
    database="state", projection="lambert", par=LCC.parallels, plot=FALSE)
  #                  parameters to lambert: ^^^^^^^^^^^^^^^^^
  #                  see mapproj::mapproject
  map.IOAPI$x <- map.lines.coords.IOAPI.x + extents@xmin # or extents.xmin?
  map.IOAPI$y <- map.lines.coords.IOAPI.y + extents@ymin # or extents.ymin?
  map.IOAPI$range <- c(
    min(map.IOAPI$x),
    max(map.IOAPI$x),
    min(map.IOAPI$y),
    max(map.IOAPI$y))
  map.IOAPI.shp <-
    maptools::map2SpatialLines(map.IOAPI, proj4string=CRS)

  # # start debugging
  # class(map.IOAPI.shp)
  # # [1] "SpatialLines"
  # # attr(,"package")
  # # [1] "sp"
  # bbox(map.IOAPI.shp)
  # #        min     max
  # # x -2272238 2322260
  # # y -1567156 1265778
  # # compare to (above)
  # # > template.extents
  # # class       : Extent 
  # # xmin        : -2556000 
  # # xmax        : 2952000 
  # # ymin        : -1728000 
  # # ymax        : 1860000 
  # #   end debugging

  return(map.IOAPI.shp)
} # end project.M3.boundaries.for.CMAQ <- function(

# ----------------------------------------------------------------------

# see `project.M3.boundaries.for.CMAQ` above
project.US.state.boundaries.for.CMAQ <- function(
  units='m',
  extents.fp,
  extents,
  LCC.parallels=c(33,45),
  CRS
) {
  return(
    project.M3.boundaries.for.CMAQ(
      database='state',
      units,
      extents.fp,
      extents,
      LCC.parallels,
      CRS)
  )
} # end project.US.state.boundaries.for.CMAQ <- function(

# ----------------------------------------------------------------------

# see `project.M3.boundaries.for.CMAQ` above
project.NorAm.boundaries.for.CMAQ <- function(
  units='m',
  extents.fp,
  extents,
  LCC.parallels=c(33,45),
  CRS
) {
  return(
    project.M3.boundaries.for.CMAQ(
      database='canusamex',
      units,
      extents.fp,
      extents,
      LCC.parallels,
      CRS)
  )
} # end project.NorAm.boundaries.for.CMAQ <- function(

# ----------------------------------------------------------------------

nonplot.vis.layer <- function(
  nc.fp,         # path to netCDF datasource ...
  datavar.name,  # name of the netCDF data variable
  sigdigs=3      # precision to use for stats
) {
  # ncdump the file
  cmd <- sprintf('ncdump -h %s', nc.fp)
  cat(sprintf('%s==\n', cmd))
  system(cmd)
  # get stats on it
  netCDF.stats.to.stdout(
    netcdf.fp=nc.fp,
    data.var.name=datavar.name,
    stats.precision=sigdigs)
} # end nonplot.vis.layer <- function

# ----------------------------------------------------------------------

plot.vis.layer <- function(
  layer,         # ... for data (a RasterLayer)
  map.list,      # maps to overlay on data: list of SpatialLines or objects castable thereto 
  pdf.fp,        # path to PDF output
  pdf.height,
  pdf.width
) {
  # start plot driver
  cat(sprintf(
    '%s: plotting %s (may take awhile! TODO: add progress control)\n',
    'plot.vis.layer', pdf.fp))
  pdf(file=pdf.fp, width=pdf.width, height=pdf.height)

  library(rasterVis)

  # I want to overlay the data with each map in the list, iteratively.
  # But the following does not work: only the last map in the list shows.
#  plots <- rasterVis::levelplot(layer,
#  #  layers,
#    margin=FALSE,
#  #  names.attr=names(global.df),
#    layout=c(1,length(names(layer))))
#  for (i.map in 1:length(map.list)) {
#    plots <- plots + latticeExtra::layer(
#      # why does this fail if map.shp is local? see 'KLUDGE' in callers :-(
#      sp::sp.lines(
#        as(map.list[[i.map]], "SpatialLines"),
#        lwd=0.8, col='darkgray'))
#  } # end for (i.map in 1:length(map.list))
#  plot(plots)

  # For now, kludge :-( handle lists of length 1 and 2 separately:
  if        (length(map.list) == 1) {

    plot(
      rasterVis::levelplot(layer,
        margin=FALSE,
#        layout=c(1,length(names(layer)))
      ) + latticeExtra::layer(
        sp::sp.lines(
          as(map.list[[1]], "SpatialLines"),
          lwd=0.8, col='darkgray')
      )
    )

  } else if (length(map.list) == 2) {

    plot(
      rasterVis::levelplot(layer,
        margin=FALSE,
#        layout=c(1,length(names(layer)))
      ) + latticeExtra::layer(
        sp::sp.lines(
          as(map.list[[1]], "SpatialLines"),
          lwd=0.8, col='darkgray')
      ) + latticeExtra::layer(
        sp::sp.lines(
          as(map.list[[2]], "SpatialLines"),
          lwd=0.8, col='darkgray')
      )
    )

  } else {
    stop(sprintf('plot.vis.layer: ERROR: cannot handle (length(map.list)==%i', length(map.list)))
  }
  
  dev.off()

} # end plot.vis.layer <- function

# ----------------------------------------------------------------------

visualize.layer <- function(
  nc.fp,         # path to netCDF datasource ...
  datavar.name,  # name of the netCDF data variable
  sigdigs=3,     # precision to use for stats
  layer,         # ... for data (a RasterLayer)
  map.list,      # maps to overlay on data: list of SpatialLines or objects castable thereto 
  pdf.fp,        # path to PDF output
  pdf.height,
  pdf.width
) {
  nonplot.vis.layer(nc.fp, datavar.name, sigdigs)
  plot.vis.layer(layer, map.list, pdf.fp, pdf.height, pdf.width)
} # end visualize.layer <- function

# ----------------------------------------------------------------------

nonplot.vis.layers <- function(
  nc.fp,           # path to netCDF datasource ...
  datavar.name,    # name of the netCDF data variable
  layer.dim.name,  # name of the datavar dimension indexing the layers (e.g., 'time')
  sigdigs=3        # precision to use for stats
) {
  # ncdump the file
  cmd <- sprintf('ncdump -h %s', nc.fp)
  cat(sprintf('%s==\n', cmd))
  system(cmd)
  # get stats on it
  # until netCDF.stats.to.stdout.by.timestep supports range restriction, just get all timesteps
  netCDF.stats.to.stdout.by.timestep(
    netcdf.fp=nc.fp,
    data.var.name=datavar.name,
    time.dim.name=layer.dim.name,
    stats.precision=sigdigs)

#  # TODO: make these work as if evaluated @ top level
#  brick          # show it
#  summary(brick) # compare to netCDF.stats above
} # end nonplot.vis.layers <- function

# ----------------------------------------------------------------------

plot.vis.layers <- function(
  brick,    # ... for data (a RasterBrick)
  map.list, # maps to overlay on data: list of SpatialLines or objects castable thereto 
  pdf.fp,   # path to PDF output
  pdf.height,
  pdf.width
) {
  # start plot driver
  cat(sprintf(
    '%s: plotting %s (may take awhile! TODO: add progress control)\n',
    'plot.vis.layers', pdf.fp))
  pdf(file=pdf.fp, width=pdf.width, height=pdf.height)

  library(rasterVis)

  # I want to overlay the data with each map in the list, iteratively.
  # But the following does not work: only the last map in the list shows.
#  plots <- rasterVis::levelplot(brick,
#  #  layers,
#    margin=FALSE,
#  #  names.attr=names(global.df),
#    layout=c(1,length(names(brick))))
#  for (i.map in 1:length(map.list)) {
#    plots <- plots + latticeExtra::layer(
#      # why does this fail if map.shp is local? see 'KLUDGE' in callers :-(
#      sp::sp.lines(
#        as(map.list[[i.map]], "SpatialLines"),
#        lwd=0.8, col='darkgray'))
#  } # end for (i.map in 1:length(map.list))
#  plot(plots)

  # For now, kludge :-( handle lists of length 1 and 2 separately:
  if        (length(map.list) == 1) {

    plot(
      rasterVis::levelplot(brick,
        margin=FALSE,
        layout=c(1,length(names(brick)))
      ) + latticeExtra::layer(
        sp::sp.lines(
          as(map.list[[1]], "SpatialLines"),
          lwd=0.8, col='darkgray')
      )
    )

  } else if (length(map.list) == 2) {

    plot(
      rasterVis::levelplot(brick,
        margin=FALSE,
        layout=c(1,length(names(brick)))
      ) + latticeExtra::layer(
        sp::sp.lines(
          as(map.list[[1]], "SpatialLines"),
          lwd=0.8, col='darkgray')
      ) + latticeExtra::layer(
        sp::sp.lines(
          as(map.list[[2]], "SpatialLines"),
          lwd=0.8, col='darkgray')
      )
    )

  } else {
    stop(sprintf('plot.vis.layers: ERROR: cannot handle (length(map.list)==%i', length(map.list)))
  }
  
  dev.off()

} # end plot.vis.layers <- function

# ----------------------------------------------------------------------

visualize.layers <- function(
  nc.fp,           # path to netCDF datasource ...
  datavar.name,    # name of the netCDF data variable
  layer.dim.name,  # name of the datavar dimension indexing the layers (e.g., 'time')
  sigdigs=3,       # precision to use for stats
  brick,           # ... for data (a RasterBrick)
  map.list, # maps to overlay on data: list of SpatialLines or objects castable thereto 
  pdf.fp,          # path to PDF output
  pdf.height,
  pdf.width
) {
  nonplot.vis.layers(nc.fp, datavar.name, layer.dim.name, sigdigs)
  plot.vis.layers(brick, map.list, pdf.fp, pdf.height, pdf.width)
} # end visualize.layers <- function
