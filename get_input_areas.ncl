;!/usr/bin/env ncl ; requires version >= ???
;;; ----------------------------------------------------------------------
;;; Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

;;; This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

;;; * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

;;; * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

;;; This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
;;; ----------------------------------------------------------------------

;;; Get areas of GFED/global gridcells from a sample file with the desired schema.

;----------------------------------------------------------------------
; libraries
;----------------------------------------------------------------------

;;; not needed in NCL versions >= 6.2.0 : see http://www.ncl.ucar.edu/current_release.shtml#LoadingScripts6.2.0
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"    ; all built-ins?
;load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl" ; for stat_dispersion

;----------------------------------------------------------------------
; functions
;----------------------------------------------------------------------

; Calculate area bounded by lons and lats on sphere, as defined by input
; * variable with coord vars=(lat, lon) (i.e., with those names and that order)
;   defining the _centers_ of a grid
; * radius of sphere
; Returns area as float(lat,lon), but assumes input lat,lon==double.
undef("lats_lons_sphere_area")  ; allow redefinition in console
function lats_lons_sphere_area(
  lat_lon_var [*][*] : numeric, ; 2D
  radius      [1]    : numeric
)
  local Pi, radius_2, sphere_area,\
    lat_var, lat_size, lat_lines_size, lat_lines, lat_span, i_lat,\
    lon_var, lon_size, lon_lines_size, lon_lines, lon_span, i_lon    
begin

  Pi = 3.14159265 ; why do I hafta define this?
  radius_2 = radius ^ 2 ; will also use later
  sphere_area = 4 * Pi * radius_2 ; for debugging only

  ;;; get dimensions/coords of passed grid centers var
  lat_var = lat_lon_var&lat
  lat_size = dimsizes(lat_var)
  lat_lines_size = lat_size + 1
  lon_var = lat_lon_var&lon
  lon_size = dimsizes(lon_var)
  lon_lines_size = lon_size + 1

  ;;; create dimensions/coords for grid _lines_ var
  lat_lines=new( (/ lat_lines_size /), double)
  lon_lines=new( (/ lon_lines_size /), double)
  ; assume grid fully spans globe, and grid lines are evenly separated
  lat_span = 180.0/lat_size ; latitudinal span of each gridcell
  lon_span = 360.0/lon_size ; longitudinal span of each gridcell

  ; assume `lat` points to grid centers, with "innermost" line a half span away from first center.
  lat_lines(0) = lat_var(0) - (lat_span/2.0)
  do i_lat = 1 , lat_size
    lat_lines(i_lat) = lat_lines(i_lat - 1) + lat_span 
;    print("lat_lines("+i_lat+")="+lat_lines(i_lat))
  end do

  ; assume `lon` points to grid centers, with "leftmost" line a half span away from first center.
  lon_lines(0) = lon_var(0) - (lon_span/2.0)
  do i_lon = 1 , lon_size
    lon_lines(i_lon) = lon_lines(i_lon - 1) + lon_span 
;    print("lon_lines("+i_lon+")="+lon_lines(i_lon))
  end do

;  print("lats_lons_sphere_area: calculating area array")
  ; keep dimension order
  area_arr = new( (/ lat_size, lon_size /), float)
  do i_lat = 0 , lat_size - 1
    do i_lon = 0 , lon_size - 1
      ; `gc_qarea` is built-in!
      area_arr(i_lat, i_lon) = tofloat(gc_qarea(\
        (/ lat_lines(i_lat), lat_lines(i_lat+1), lat_lines(i_lat+1), lat_lines(i_lat)   /),\
        (/ lon_lines(i_lon), lon_lines(i_lon),   lon_lines(i_lon+1), lon_lines(i_lon+1) /)\
      ) * radius_2)
    end do ; iterate lon
  end do ; iterate lat

;  ; start debugging
;  print("sum(area_arr)="+sum(area_arr)+", sphere_area="+sphere_area)
;  print("sum(area_arr)/sphere_area="+sum(area_arr)/sphere_area)
;  ; (0)   sum(area_arr)=5.09889e+14, sphere_area=5.09904e+14
;  ; (0)   sum(area_arr)/sphere_area=0.99997
;  ;   end debugging

  return(area_arr) ; as float
end ; lats_lons_sphere_area

;----------------------------------------------------------------------
; code
;----------------------------------------------------------------------

begin ; skip if copy/paste-ing to console

;----------------------------------------------------------------------
; constants
;----------------------------------------------------------------------

  ;;; model/run constants
  this_fn = getenv("GET_INPUT_AREAS_FN") ; for debugging
  model_year = stringtoint(getenv("MODEL_YEAR"))
  CMAQ_radius = stringtofloat(getenv("CMAQ_RADIUS"))

  ;;; input constants
  in_fp = getenv("GFED_AREAS_GLOBAL_SAMPLE_FP")
  in_datavar_name = getenv("GFED_AREAS_GLOBAL_SAMPLE_DATAVAR_NAME")
  in_dim_row_name = getenv("GFED_AREAS_GLOBAL_SAMPLE_DIM_ROW_NAME")
  in_dim_col_name = getenv("GFED_AREAS_GLOBAL_SAMPLE_DIM_COL_NAME")

  ;;; output constants
  out_fp = getenv("GFED_AREAS_GLOBAL_FP")
  out_history = getenv("GFED_AREAS_GLOBAL_HISTORY")
  out_datavar_name = getenv("GFED_AREAS_GLOBAL_DATAVAR_NAME")
  out_datavar_long_name = getenv("GFED_AREAS_GLOBAL_DATAVAR_LONG_NAME")
  out_datavar_standard_name = getenv("GFED_AREAS_GLOBAL_DATAVAR_STANDARD_NAME")
  out_datavar_units = getenv("GFED_AREAS_GLOBAL_DATAVAR_UNITS")
  out_dim_row_name = getenv("GFED_AREAS_GLOBAL_DIM_ROW_NAME")
  out_dim_col_name = getenv("GFED_AREAS_GLOBAL_DIM_COL_NAME")

;----------------------------------------------------------------------
; payload
;----------------------------------------------------------------------

  ;;; workaround if `source`ing driver? e.g.,
  ; in_fh = addfile("."+in_fp, "r")  ; file handle
  ; out_fh = addfile("."+out_fp, "c")  ; file handle

  ;;; get sample input data
  in_fh = addfile(in_fp, "r") ; use workaround if `source`ing driver
  in_datavar = in_fh->$in_datavar_name$
;  lat_lon_arr = in_datavar(:,:)
;  sample_areas = lats_lons_sphere_area(lat_lon_arr, CMAQ_radius)
  sample_areas = lats_lons_sphere_area(in_datavar, CMAQ_radius)

  ; start debugging---------------------------------------------------
  printVarSummary(sample_areas)
  in_stats = stat_dispersion(sample_areas, False) ; False == don't stdout
  print(\
    str_get_nl()                                               + \
    "gridcell areas (m^2) from " + in_fp + ":" + str_get_nl()  + \
    "|cells|=" + in_stats(18)                   + str_get_nl() + \
    "|obs|  =" + in_stats(19)                   + str_get_nl() + \
    "min    =" + sprintf("%7.3e", in_stats(2))  + str_get_nl() + \
    "q1     =" + sprintf("%7.3e", in_stats(6))  + str_get_nl() + \
    "med    =" + sprintf("%7.3e", in_stats(8))  + str_get_nl() + \
    "mean   =" + sprintf("%7.3e", in_stats(0))  + str_get_nl() + \
    "q3     =" + sprintf("%7.3e", in_stats(10)) + str_get_nl() + \
    "max    =" + sprintf("%7.3e", in_stats(14)) + str_get_nl() + \
    str_get_nl() )
  ;   end debugging---------------------------------------------------

  ;;; create output data file
  out_fh = addfile(out_fp, "c") ; use workaround if `source`ing driver

  ;; define global attributes
  out_fh_att = True ; assign file/global attributes
  out_fh_att@history = out_history
  out_fh_att@creation_date = systemfunc("date") ; the canonical way
  
  ;; create file data variable (datavar) and its coordinate variables
  ; fill data
  temp_var = sample_areas(:,:) ; pass-by-value, I hope
  ; add datavar attributes
  temp_var@long_name = out_datavar_long_name
  temp_var@standard_name = out_datavar_standard_name
  temp_var@units = out_datavar_units
  ; name dimensions, provide coordinates
  temp_var!0 = out_dim_row_name
  temp_var&$out_dim_row_name$ = in_fh->$in_dim_row_name$
  temp_var!1 = out_dim_col_name
  temp_var&$out_dim_col_name$ = in_fh->$in_dim_col_name$
  ; write datavar
  out_fh->$out_datavar_name$ = temp_var
  ; write coordinate attributes: must do this after writing datavar
  out_fh->$out_dim_row_name$@standard_name = out_fh->$out_dim_row_name$@long_name
  out_fh->$out_dim_col_name$@standard_name = out_fh->$out_dim_col_name$@long_name
  
  ;; write global attributes: can do this after writing datavar
  fileattdef(out_fh, out_fh_att)

  ; start debugging---------------------------------------------------
  out_datavar = out_fh->$out_datavar_name$
  printVarSummary(out_datavar)
  out_stats = stat_dispersion(sample_areas, False) ; False == don't stdout
  print(\
    str_get_nl()                                                + \
    "gridcell areas (m^2) from " + out_fp + ":" + str_get_nl()  + \
    "|cells|=" + out_stats(18)                   + str_get_nl() + \
    "|obs|  =" + out_stats(19)                   + str_get_nl() + \
    "min    =" + sprintf("%7.3e", out_stats(2))  + str_get_nl() + \
    "q1     =" + sprintf("%7.3e", out_stats(6))  + str_get_nl() + \
    "med    =" + sprintf("%7.3e", out_stats(8))  + str_get_nl() + \
    "mean   =" + sprintf("%7.3e", out_stats(0))  + str_get_nl() + \
    "q3     =" + sprintf("%7.3e", out_stats(10)) + str_get_nl() + \
    "max    =" + sprintf("%7.3e", out_stats(14)) + str_get_nl() + \
    str_get_nl() )

  printVarSummary(out_fh)
  print(systemfunc("ncdump -h "+out_fp))
  ;   end debugging---------------------------------------------------

end ; get_input_areas.ncl
