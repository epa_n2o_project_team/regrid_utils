#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

### Just a utility to scan status of regridding repos: NOT part of the build/run.
### See also the similarly-named tools in repo=CMAQ-build. TODO: refactor.

REPO_ROOT="${HOME}/code/regridding" # on terrae
REPO_CMD="git status | grep -ve '\.gz$\|\.log$\|\.nc$\|\.ncf$\|\.pdf$\|\.txt$\|\.zip$'"

# ASSERT: same as repo_branch.sh from here out
for REPO_DIR in $(find ${REPO_ROOT}/ -maxdepth 1 -type d | grep -ve '/$' | grep -ve 'BLD_\|RUN_' | sort) ; do
#  echo -e "REPO_DIR='${REPO_DIR}'"
  for CMD in \
    "pushd ${REPO_DIR}" \
    "${REPO_CMD}" \
    "popd" \
  ; do
    echo -e "$ ${CMD}"
    eval "${CMD}"
  done
  echo # newline
done
