*(part of the [AQMEII-NA_N2O][AQMEII-NA_N2O wiki home] family of projects)*

**table of contents**

[TOC]

# open-source notice

Copyright 2013, 2014 Tom Roche <Tom_Roche@pobox.com>

This project's content is free software: you can redistribute it and/or modify it provided that you do so as follows:

* under the terms of the [GNU Affero General Public License][GNU Affero General Public License HTML @ GNU] as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
* preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

This project's content is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the [GNU Affero General Public License][GNU Affero General Public License local text] for more details.

![distributed under the GNU Affero General Public License](../../downloads/Affero_badge__agplv3-155x51.png)

[GNU Affero General Public License local text]: ./COPYING
[GNU Affero General Public License HTML @ GNU]: https://www.gnu.org/licenses/agpl.html

# summary

Proto-package (hopefully the seed for "real" [NCL][NCL @ wikipedia] and [R][R @ wikipedia] packages, maybe even [bash][bash @ wikipedia]) for management of "regrid helpers" used by other projects, e.g.,

* [`AQMEII-NA_N2O_integration`][AQMEII-NA_N2O_integration]
* [`AQMEII_ag_soil`][AQMEII_ag_soil]
* [`CLM_CN_global_to_AQMEII-NA`][CLM_CN_global_to_AQMEII-NA]
* [`CMAQ_BC_IC_plus_N2O`][CMAQ_BC_IC_plus_N2O]
* [`EDGAR-4.2_minus_soil_and_biomass_to_AQMEII-NA`][EDGAR-4.2_minus_soil_and_biomass_to_AQMEII-NA]
* [`GEIA_regrid`][GEIA_regrid]
* [`GFED-3.1_global_to_AQMEII-NA`][GFED-3.1_global_to_AQMEII-NA]
* [`MOZART-global_to_AQMEII-NA`][MOZART-global_to_AQMEII-NA]

[AQMEII-NA_N2O wiki home]: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/Home
[AQMEII-NA_N2O_integration]: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o_integration
[AQMEII_ag_soil]: https://bitbucket.org/epa_n2o_project_team/aqmeii_ag_soil
[CLM_CN_global_to_AQMEII-NA]: https://bitbucket.org/epa_n2o_project_team/clm_cn_global_to_aqmeii-na
[CMAQ_BC_IC_plus_N2O]: https://bitbucket.org/epa_n2o_project_team/cmaq_bc_ic_plus_n2o
[EDGAR-4.2_minus_soil_and_biomass_to_AQMEII-NA]: https://bitbucket.org/epa_n2o_project_team/edgar-4.2_minus_soil_and_biomass_to_aqmeii-na
[GEIA_regrid]: https://bitbucket.org/epa_n2o_project_team/geia_regrid
[GFED-3.1_global_to_AQMEII-NA]: https://bitbucket.org/epa_n2o_project_team/gfed-3.1_global_to_aqmeii-na
[MOZART-global_to_AQMEII-NA]: https://bitbucket.org/epa_n2o_project_team/mozart-global-to-aqmeii-na
[bash @ wikipedia]: http://en.wikipedia.org/wiki/Bash_%28Unix_shell%29
[R @ wikipedia]: http://en.wikipedia.org/wiki/R_%28programming_language%29
[NCL @ wikipedia]: http://en.wikipedia.org/wiki/NCAR_Command_Language

# TODOs

1. Document functionality in this page!
1. Move all these TODOs to [our issue tracker][regrid_utils issues].
1. `*.sh`: use bash booleans à la [`N2O_integration_driver.sh`][AQMEII-NA_N2O_integration/N2O_integration_driver.sh].
1. Handle {name, path to} VERDI in [`bash_utilities.sh`][bash_utilities.sh].
1. *(see [`summarize.ncl`][summarize.ncl])* Put `stat_dispersion` bug on [NCL][NCL @ wikipedia].
1. *(see [`summarize.ncl`][summarize.ncl])* Make all `summarize*` functions take `{title, sigdigs}` like `summarize_array(...)`.
1. Make commandline [`netCDF.stats.to.stdout.r`][netCDF.stats.to.stdout.r] take files beginning with numerals, e.g.
    * (-) `Rscript ./netCDF.stats.to.stdout.r netcdf.fp=5yravg_20111219_pure_emis.nc data.var.name=N2O # fails`
    * (+) `Rscript ./netCDF.stats.to.stdout.r netcdf.fp=yravg_20111219_pure_emis.nc data.var.name=N2O # works`
1. Fully document platform versions (e.g., linux, compilers, bash, NCL, R).
1. Test on
    * tlrPanP5 (which now has R package=ncdf4, but readAsciiTable of input .txt's is very slow compared to terrae)
    * HPCC (once problem with ncdf4 on amad1 is debugged: in process with JOB and KMF)
    * EMVL: terrae and sol

[regrid_utils issues]: ../../issues
[bash_utilities.sh]: ../../src/HEAD/bash_utilities.sh?at=master
[summarize.ncl]: ../../src/HEAD/summarize.ncl?at=master
[netCDF.stats.to.stdout.r]: ../../src/HEAD/netCDF.stats.to.stdout.r?at=master
[AQMEII-NA_N2O_integration/N2O_integration_driver.sh]: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o_integration/src/HEAD/N2O_integration_driver.sh?at=master
